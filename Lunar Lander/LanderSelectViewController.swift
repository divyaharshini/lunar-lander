//
//  SecondViewController.swift
//  Lunar Lander
//
//  Created by rayaan on 23/02/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class LanderSelectViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var imageArray = [UIImage(named : "lander_apollo_11"),UIImage(named : "lander_craft_1"),UIImage(named : "lander3"),UIImage(named : "image4")]
    
    var imageName = UIImage(named: "lander_apollo_11")
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LandersCollectionViewCell", for: indexPath) as! LandersCollectionViewCell
        cell.landerImage.image = imageArray[indexPath.row]
        
        return cell
    }
    
    //Collection view didselected item
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageName = imageArray[indexPath.row]
        LanderImage.shared.setLanderImage(landerImage: imageName!)
        showToast(message: "Craft Selected")
    }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/4 - 75, y: self.view.frame.size.height-200, width: 350, height: 36))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .left;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.textAlignment = NSTextAlignment.center
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }//end of show toast method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
}

