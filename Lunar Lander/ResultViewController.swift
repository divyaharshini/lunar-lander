//
//  ResultViewController.swift
//  Lunar Lander
//
//  Created by rayaan on 11/03/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        result()//calling result method
        
        // Do any additional setup after loading the view.
    }//end of view did load method
    
    @IBOutlet weak var resultLBL: UILabel!
    @IBOutlet weak var fuelRemainingLBL: UILabel!
    @IBOutlet weak var timeTakenLBL: UILabel!
    @IBOutlet weak var touchDownVelocityLBL: UILabel!
    @IBOutlet weak var surfaceImage: UIImageView!
    
    @IBAction func startAgainBTN(_ sender: Any) {
        
        Calculations.shared.fuel = 450
        Calculations.shared.time = 0
        Calculations.shared.height = 1750
        Calculations.shared.velocity = 16.0
        Calculations.shared.touchdownVelocity = 0.0
        Calculations.shared.temporaryThrust = 0.0
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }//end of start again button
    
    func result(){
        surfaceImage.image = SurfaceImage.shared.getSurfaceImage()
        resultLBL.text = String(Calculations.shared.result())
        fuelRemainingLBL.text = String("\(Calculations.shared.fuel) kg")
        timeTakenLBL.text = String("\(Calculations.shared.time) s")
        touchDownVelocityLBL.text = String(format:"%.2f m/s",Calculations.shared.touchdownVelocity)
    }//end of function result
    
    override func viewDidAppear(_ animated: Bool) {
        print("I will load everytime user view this page")
    }//end of view will appear method
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}//end of class
